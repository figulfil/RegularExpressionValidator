using System.Linq;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using RegularExpressionValidator.Client;
using RegularExpressionValidator.Client.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddHttpClient("RegularExpressionValidator.ServerAPI", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
    .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

// Supply HttpClient instances that include access tokens when making requests to the server project
builder.Services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("RegularExpressionValidator.ServerAPI"));

builder.Services.AddApiAuthorization();

HttpClient httpClient = new HttpClient();
httpClient.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);

NotificationService notificationService = new NotificationService();
builder.Services.AddSingleton(notificationService);
builder.Services.AddSingleton(new ContentService(httpClient, notificationService));
builder.Services.AddSingleton(new PatternService(httpClient, notificationService));
builder.Services.AddSingleton(new UserDetailService(httpClient, notificationService));

await builder.Build().RunAsync();
