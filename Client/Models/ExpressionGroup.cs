﻿using System.Text.RegularExpressions;

namespace RegularExpressionValidator.Client.Models
{
    /// <summary>
    /// Wrapper for matches from Regex library
    /// </summary>
    public class ExpressionGroup
	{
        /// <summary>
        /// Index of group
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Depth of group
        /// </summary>
        public int Depth { get; set; }

        /// <summary>
        /// Group value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The color palette singleton
        /// </summary>
        private readonly string color = ColorPalette.Current.GetColor();

        /// <summary>
        /// Gets or sets the expression groups.
        /// </summary>
        /// <value>
        /// The expression groups.
        /// </value>
        public List<ExpressionGroup> ExpressionGroups { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpressionGroup"/> class.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <param name="matchOffset">The match offset.</param>
        public ExpressionGroup(Group group, int matchOffset)
		{
			this.Index = group.Index - matchOffset;
			this.Value = group.Value;
			this.ExpressionGroups = new List<ExpressionGroup>();
		}

        /// <summary>
        /// Determines whether the specified group contains group.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <returns>
        ///   <c>true</c> if the specified group contains group; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsGroup(ExpressionGroup group) => this.Index <= group.Index && group.Index < this.Index + this.Value.Length;

        /// <summary>
        /// Adds the group.
        /// </summary>
        /// <param name="newGroup">The new group.</param>
        /// <param name="depth">The depth.</param>
        public void AddGroup(ExpressionGroup newGroup, int depth = 0)
		{
			this.Depth = depth;
			bool createNewGroup = true;

			foreach(ExpressionGroup childGroup in ExpressionGroups)
			{
				if(childGroup.ContainsGroup(newGroup))
				{
					childGroup.AddGroup(newGroup, this.Depth + 1);
					createNewGroup = false;
					break;
				}
			}

			if(createNewGroup)
			{
				newGroup.Depth = this.Depth + 1;
				ExpressionGroups.Add(newGroup);
			}
		}

        /// <summary>
        /// Lists by depth of graph.
        /// </summary>
        /// <param name="depth">The depth.</param>
        /// <returns></returns>
        public List<ExpressionGroup> ToListByDepth(int depth)
		{
			List<ExpressionGroup> output = new List<ExpressionGroup>();

			foreach(ExpressionGroup group in this.ToList())
				if(group.Depth == depth)
					output.Add(group);

			return output;
		}

        /// <summary>
        /// Converts to list.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public List<ExpressionGroup> ToList(List<ExpressionGroup> list = null)
		{
			if (list == null)
				list = new List<ExpressionGroup>();

			list.Add(this);

			foreach (ExpressionGroup group in ExpressionGroups)
				group.ToList(list);

			return list;
		}

        /// <summary>
        /// Converts to html with color.
        /// </summary>
        /// <returns></returns>
        public string ToHtml()
		{
			string html = this.Value;
			int offset = 0;

			foreach(ExpressionGroup group in ExpressionGroups)
			{
				string snippet = group.ToHtml();
				
				html = html.Remove(group.Index + offset, group.Value.Length);
				html = html.Insert(group.Index + offset, snippet);

				offset += snippet.Length - group.Value.Length;
			}

			return @$"
					<div class='node' style='background-color: {color}'>
						{html}
					</div>
					";
		}
	}
}
