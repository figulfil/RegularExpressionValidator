﻿namespace RegularExpressionValidator.Client.Models
{
    /// <summary>
    /// Notification class that is displayed in notification
    /// </summary>
    public class Notification
    {
        /// <summary>
        /// Types of notificaitons
        /// </summary>
        public enum NotificationType
        {
            Success,
            Error
        }

        /// <summary>
        /// Heading of notification
        /// </summary>
        public string Heading { get; set; }

        /// <summary>
        /// Message of notification
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Css class of notification
        /// </summary>
        public string ClassCss { get; set; }

        /// <summary>
        /// Css icon of notification
        /// </summary>
        public string IconCss { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Notification"/> class.
        /// </summary>
        /// <param name="Message">The message.</param>
        /// <param name="type">The type.</param>
        public Notification(string Message, NotificationType type)
        {
            this.Message = Message;

            switch(type)
            {
                case NotificationType.Success:
                    this.Heading = "Success";
                    this.ClassCss = "notification-success";
                    this.IconCss = "circle-check";
                    break;
                case NotificationType.Error:
                    this.Heading = "Error";
                    this.ClassCss = "notification-error";
                    this.IconCss = "circle-x";
                    break;
            }
        }
    }
}
