﻿namespace RegularExpressionValidator.Client.Models
{
    /// <summary>
    /// Class wrapping our color schemas
    /// </summary>
    public sealed class ColorPalette
    {
        /// <summary>
        /// Implementation of singleton pattern
        /// </summary>
        private static ColorPalette current = null;

        private ColorPalette() { }

        public static ColorPalette Current
        {
            get
            {
                if (current == null)
                {
                    current = new ColorPalette();
                }

                return current;
            }
        }

        private int index = 0;

        private readonly List<string> colors = new List<string>
        {
            "#CBE4F9",
            "#EFF9DA",
            "#F9EBDF",
            "#CDF5F6",
            "#F9D8D6",
            "#D6CDEA"
        };

        /// <summary>
        /// Gets the color.
        /// </summary>
        /// <returns></returns>
        public string GetColor()
        {
            return colors[index++ % colors.Count];
        }
    }
}
