﻿using RegularExpressionValidator.Client.Models;
using RegularExpressionValidator.Shared.Models;
using System.Net;
using System.Net.Http.Json;

namespace RegularExpressionValidator.Client.Services
{
    /// <summary>
    /// Service responsible for managintg user detail data
    /// </summary>
    /// <seealso cref="RegularExpressionValidator.Client.Services.IEntityService&lt;RegularExpressionValidator.Shared.Models.UserDetail&gt;" />
    public class UserDetailService : IEntityService<UserDetail>
    {
        /// <summary>
        /// Gets the injected HTTP client.
        /// </summary>
        public HttpClient HttpClient { get; set; }

        /// <summary>
        /// Gets the injected notification service.
        /// </summary>
        public NotificationService NotificationService { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDetailService"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        /// <param name="notificationService">The notification service.</param>
        public UserDetailService(HttpClient httpClient, NotificationService notificationService)
        {
            this.HttpClient = httpClient;
            this.NotificationService = notificationService;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Create(UserDetail entity)
        {
            return Notify("UserDetail was successfully created", "UserDetail could not be created", await HttpClient.PostAsJsonAsync("api/UserDetails", entity));
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Delete(int id)
        {
            return Notify("UserDetail was successfully deleted", "UserDetail could not be deleted", await HttpClient.DeleteAsync($"api/UserDetails/{id}"));
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserDetail>> ListAll()
        {
            return await HttpClient.GetFromJsonAsync<List<UserDetail>>("api/UserDetails");
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<UserDetail> Read(int id)
        {
            return await HttpClient.GetFromJsonAsync<UserDetail>($"api/UserDetails/{id}");
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Update(UserDetail entity)
        {
            return Notify("UserDetail was successfully updated", "UserDetail could not be updated", await HttpClient.PutAsJsonAsync("api/UserDetails", entity));
        }

        /// <summary>
        /// Reads the by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public async Task<UserDetail> ReadByUserId(int userId)
        {
            return await HttpClient.GetFromJsonAsync<UserDetail>($"api/UserDetails/User/{userId}");
        }

        /// <summary>
        /// Notifies the specified success message.
        /// </summary>
        /// <param name="successMessage">The success message.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public HttpStatusCode Notify(string successMessage, string errorMessage, HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                NotificationService.ShowNotification(new Notification(successMessage, Notification.NotificationType.Success));
            else
                NotificationService.ShowNotification(new Notification(errorMessage, Notification.NotificationType.Error));

            return response.StatusCode;
        }
    }
}
