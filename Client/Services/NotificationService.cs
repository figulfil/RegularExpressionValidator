﻿using RegularExpressionValidator.Client.Models;

namespace RegularExpressionValidator.Client.Services
{
    /// <summary>
    /// Service responsible for managing notification
    /// </summary>
    public class NotificationService
    {
        /// <summary>
        /// Delegate containing show method of notification component
        /// </summary>
        public event Action<Notification> Show;

        /// <summary>
        /// Shows the notification.
        /// </summary>
        /// <param name="notification">The notification.</param>
        public void ShowNotification(Notification notification)
        {
            Show.Invoke(notification);
        }
    }
}
