﻿using Microsoft.AspNetCore.Components;
using RegularExpressionValidator.Client.Models;
using RegularExpressionValidator.Shared.Models;
using System.Net;
using System.Net.Http.Json;

namespace RegularExpressionValidator.Client.Services
{
    /// <summary>
    /// Service responsible for managintg content data
    /// </summary>
    /// <seealso cref="RegularExpressionValidator.Client.Services.IEntityService&lt;RegularExpressionValidator.Shared.Models.Content&gt;" />
    public class ContentService : IEntityService<Content>
    {
        /// <summary>
        /// Gets the injected HTTP client.
        /// </summary>
        public HttpClient HttpClient { get; }

        /// <summary>
        /// Gets the injected notification service.
        /// </summary>
        public NotificationService NotificationService { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentService"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        /// <param name="notificationService">The notification service.</param>
        public ContentService(HttpClient httpClient, NotificationService notificationService)
        {
            this.HttpClient = httpClient;
            this.NotificationService = notificationService;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Create(Content entity)
        {
            return Notify("Content was successfully created", "Content could not be created", await HttpClient.PostAsJsonAsync("api/Contents", entity));
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Delete(int id)
        {
            return Notify("Content was successfully deleted", "Content could not be deleted", await HttpClient.DeleteAsync($"api/Contents/{id}"));
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Content>> ListAll()
        {
            return await HttpClient.GetFromJsonAsync<List<Content>>("api/Contents");
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Content> Read(int id)
        {
            return await HttpClient.GetFromJsonAsync<Content>($"api/Contents/{id}");
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Update(Content entity)
        {
            return Notify("Content was successfully updated", "Content could not be updated", await HttpClient.PutAsJsonAsync("api/Contents", entity));
        }

        /// <summary>
        /// Notifies the specified success message.
        /// </summary>
        /// <param name="successMessage">The success message.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public HttpStatusCode Notify(string successMessage, string errorMessage, HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                NotificationService.ShowNotification(new Notification(successMessage, Notification.NotificationType.Success));
            else
                NotificationService.ShowNotification(new Notification(errorMessage, Notification.NotificationType.Error));

            return response.StatusCode;
        }
    }
}