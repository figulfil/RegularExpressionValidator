﻿using RegularExpressionValidator.Client.Models;
using System.Net;

namespace RegularExpressionValidator.Client.Services
{
    public interface IEntityService<TEntity>
    {
        /// <summary>
        /// Gets the injected HTTP client.
        /// </summary>
        public HttpClient HttpClient { get; }

        /// <summary>
        /// Gets the injected notification service.
        /// </summary>
        public NotificationService NotificationService { get; }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public Task<HttpStatusCode> Create(TEntity entity);

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Task<TEntity> Read(int id);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public Task<HttpStatusCode> Update(TEntity entity);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Task<HttpStatusCode> Delete(int id);

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public Task<List<TEntity>> ListAll();

        /// <summary>
        /// Notifies the specified success message.
        /// </summary>
        /// <param name="successMessage">The success message.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public HttpStatusCode Notify(string successMessage, string errorMessage, HttpResponseMessage response);
    }
}