﻿using Microsoft.AspNetCore.Components;
using RegularExpressionValidator.Client.Models;
using RegularExpressionValidator.Shared.Models;
using System.Net;
using System.Net.Http.Json;

namespace RegularExpressionValidator.Client.Services
{
    /// <summary>
    /// Service responsible for managintg pattern data
    /// </summary>
    /// <seealso cref="RegularExpressionValidator.Client.Services.IEntityService&lt;RegularExpressionValidator.Shared.Models.Pattern&gt;" />
    public class PatternService : IEntityService<Pattern>
    {
        /// <summary>
        /// Gets the injected HTTP client.
        /// </summary>
        public HttpClient HttpClient { get; }

        /// <summary>
        /// Gets the injected notification service.
        /// </summary>
        public NotificationService NotificationService { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternService"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        /// <param name="notificationService">The notification service.</param>
        public PatternService(HttpClient httpClient, NotificationService notificationService)
        {
            this.HttpClient = httpClient;
            this.NotificationService = notificationService;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Create(Pattern entity)
        {
            return Notify("Pattern was successfully created", "Pattern could not be created", await HttpClient.PostAsJsonAsync("api/Patterns", entity));
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Delete(int id)
        {
            return Notify("Pattern was successfully deleted", "Pattern could not be deleted", await HttpClient.DeleteAsync($"api/Patterns/{id}"));
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Pattern>> ListAll()
        {
            return await HttpClient.GetFromJsonAsync<List<Pattern>>("api/Patterns");
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Pattern> Read(int id)
        {
            return await HttpClient.GetFromJsonAsync<Pattern>($"api/Patterns/{id}");
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Update(Pattern entity)
        {
            return Notify("Pattern was successfully updated", "Pattern could not be updated", await HttpClient.PutAsJsonAsync("api/Patterns", entity));
        }

        /// <summary>
        /// Notifies the specified success message.
        /// </summary>
        /// <param name="successMessage">The success message.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public HttpStatusCode Notify(string successMessage, string errorMessage, HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                NotificationService.ShowNotification(new Notification(successMessage, Notification.NotificationType.Success));
            else
                NotificationService.ShowNotification(new Notification(errorMessage, Notification.NotificationType.Error));

            return response.StatusCode;
        }
    }
}
