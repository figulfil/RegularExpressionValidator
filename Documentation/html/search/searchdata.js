var indexSectionsWithContent =
{
  0: ".2<abcdefghiklmnoprstuv",
  1: "acdeilnopru",
  2: "r",
  3: ".2aceilnopru",
  4: "abcdegilmnoprstu",
  5: "abcehn",
  6: "n",
  7: "es",
  8: "bcdefhiklmnprstuv",
  9: "s",
  10: "<rt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events",
  10: "Pages"
};

