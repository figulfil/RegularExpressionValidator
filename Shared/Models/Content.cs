﻿namespace RegularExpressionValidator.Shared.Models
{
    /// <summary>
    /// Class representing a text saved by user to be used for future regular expression validation
    /// </summary>
    public class Content
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public string Shortcut { get; set; }
        public int UserID { get; set; }
    }
}
