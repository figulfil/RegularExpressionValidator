﻿namespace RegularExpressionValidator.Shared.Models
{
    /// <summary>
    /// Detail of user containing his basic information
    /// </summary>
    public class UserDetail
    {
        public UserDetail()
        {
            this.Username = "";
            this.FirstName = "";
            this.LastName = "";
            this.Email = "";
            this.Description = "";
        }

        public int ID { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public DateTime? BirthDate { get; set; }
        public byte[] Picture { get; set; }
        public int UserID { get; set; }
    }
}
