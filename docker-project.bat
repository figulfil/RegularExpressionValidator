docker stop regular_expression_validator_server
docker rm regular_expression_validator_server
docker build -f Server/Dockerfile . -t regular_expression_validator_image
docker run -d --name regular_expression_validator_server --restart always --network=lan -p 8080:80 -e "ASPNETCORE_ENVIRONMENT=Docker" regular_expression_validator_image:latest