﻿using Microsoft.EntityFrameworkCore;
using RegularExpressionValidator.Server.Repositories;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Services
{
    /// <summary>
    /// Service containing bussiness logic for Content entity
    /// </summary>
    /// <seealso cref="RegularExpressionValidator.Server.Services.IService&lt;RegularExpressionValidator.Shared.Models.Content&gt;" />
    public class ContentService : IService<Content>
    {
        /// <summary>
        /// Injected repository
        /// </summary>
        private readonly ContentRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentService"/> class.
        /// </summary>
        /// <param name="ContentRepository">The content repository.</param>
        public ContentService(ContentRepository ContentRepository)
        {
            this.repository = ContentRepository;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<Content> Create(Content entity)
        {
            return await repository.Create(entity);
        }


        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Content> Read(int id)
        {
            return await repository.Read(id);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<bool> Update(Content entity)
        {
            try
            {
                await repository.Update(entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<bool> Delete(int id)
        {
            Content Content = await repository.Read(id);

            if (Content != null)
            {
                await repository.Delete(Content);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Content>> ListAll()
        {
            return await repository.ListAll();
        }
    }
}
