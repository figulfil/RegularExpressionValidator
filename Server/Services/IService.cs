﻿namespace RegularExpressionValidator.Server.Services
{
    /// <summary>
    /// Interface constraining basic CRUD operation used by services
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IService<TEntity>
    {
        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public Task<TEntity> Create(TEntity entity);

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Task<TEntity> Read(int id);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public Task<bool> Update(TEntity entity);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Task<bool> Delete(int id);

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public Task<List<TEntity>> ListAll();
    }
}
