﻿using Microsoft.EntityFrameworkCore;
using RegularExpressionValidator.Server.Repositories;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Services
{
    /// <summary>
    /// Service containing bussiness logic for UserDetail entity
    /// </summary>
    /// <seealso cref="RegularExpressionValidator.Server.Services.IService&lt;RegularExpressionValidator.Shared.Models.UserDetail&gt;" />
    public class UserDetailService : IService<UserDetail>
    {
        /// <summary>
        /// Injected repository
        /// </summary>
        private readonly UserDetailRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDetailService"/> class.
        /// </summary>
        /// <param name="UserDetailRepository">The user detail repository.</param>
        public UserDetailService(UserDetailRepository UserDetailRepository)
        {
            this.repository = UserDetailRepository;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<UserDetail> Create(UserDetail entity)
        {
            return await repository.Create(entity);
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<UserDetail> Read(int id)
        {
            return await repository.Read(id);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<bool> Update(UserDetail entity)
        {
            try
            {
                await repository.Update(entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<bool> Delete(int id)
        {
            UserDetail UserDetail = await repository.Read(id);

            if (UserDetail != null)
            {
                await repository.Delete(UserDetail);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserDetail>> ListAll()
        {
            return await repository.ListAll();
        }

        /// <summary>
        /// Reads the by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public async Task<UserDetail> ReadByUserId(int userId)
        {
            UserDetail userDetail = await repository.ReadByUserId(userId);

            if (userDetail == null)
            {
                userDetail = new UserDetail();

                userDetail.UserID = userId;
                userDetail = await Create(userDetail);
            }

            return await repository.ReadByUserId(userId);
        }
    }
}
