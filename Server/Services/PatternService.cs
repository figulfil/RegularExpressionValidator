﻿using Microsoft.EntityFrameworkCore;
using RegularExpressionValidator.Shared.Models;
using RegularExpressionValidator.Server.Repositories;

namespace RegularExpressionValidator.Server.Services
{
    /// <summary>
    /// Service containing bussiness logic for Pattern entity
    /// </summary>
    /// <seealso cref="RegularExpressionValidator.Server.Services.IService&lt;RegularExpressionValidator.Shared.Models.Pattern&gt;" />
    public class PatternService : IService<Pattern>
    {
        /// <summary>
        /// Injected repository
        /// </summary>
        private readonly PatternRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternService"/> class.
        /// </summary>
        /// <param name="patternRepository">The pattern repository.</param>
        public PatternService(PatternRepository patternRepository)
        {
            this.repository = patternRepository;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<Pattern> Create(Pattern entity)
        {
            return await repository.Create(entity);
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Pattern> Read(int id)
        {
            return await repository.Read(id);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<bool> Update(Pattern entity)
        {
            try
            {
                await repository.Update(entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<bool> Delete(int id)
        {
            Pattern pattern = await repository.Read(id);

            if (pattern != null)
            {
                await repository.Delete(pattern);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Pattern>> ListAll()
        {
            return await repository.ListAll();
        }

    }
}
