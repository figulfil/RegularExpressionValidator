using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using RegularExpressionValidator.Server.Data;
using RegularExpressionValidator.Server.Models;
using Microsoft.AspNetCore.Identity;
using RegularExpressionValidator.Server.Repositories;
using RegularExpressionValidator.Server.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseNpgsql(connectionString));

builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<ApplicationDbContext>();

builder.Services.AddIdentityServer()
    .AddApiAuthorization<User, ApplicationDbContext>()
    .AddDeveloperSigningCredential();

builder.Services.AddAuthentication()
    .AddIdentityServerJwt();

builder.Services.ConfigureExternalCookie(options => {
    options.Cookie.IsEssential = true;
    options.Cookie.SameSite = (SameSiteMode)(-1);
});

builder.Services.ConfigureApplicationCookie(options => {
    options.Cookie.IsEssential = true;
    options.Cookie.SameSite = (SameSiteMode)(-1);
});

builder.Services.AddTransient<PatternRepository>();
builder.Services.AddTransient<PatternService>();

builder.Services.AddTransient<ContentRepository>();
builder.Services.AddTransient<ContentService>();

builder.Services.AddTransient<UserDetailRepository>();
builder.Services.AddTransient<UserDetailService>();

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();

app.UseIdentityServer();
app.UseAuthentication();
app.UseAuthorization();


app.MapRazorPages();
app.MapControllers();
app.MapControllerRoute(name: "default",
               pattern: "{controller=UserDetails}/{action=User}/{userId:int}");
app.MapFallbackToFile("index.html");

app.Run();
