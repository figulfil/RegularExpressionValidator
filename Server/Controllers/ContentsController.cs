﻿using Microsoft.AspNetCore.Mvc;
using RegularExpressionValidator.Server.Services;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Controllers
{
    /// <summary>
    /// Content controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class ContentsController : ControllerBase
    {
        /// <summary>
        /// The service
        /// </summary>
        private readonly ContentService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentsController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public ContentsController(ContentService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Gets the contents.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Content>>> GetContents()
        {
            return await service.ListAll();
        }

        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Content>> GetContent(int id)
        {
            Content content = await service.Read(id);
            return content == null ? NotFound() : Ok(content);
        }

        /// <summary>
        /// Puts the content.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> PutContent(Content content)
        {
            return await service.Update(content) ? Ok(content) : NotFound();
        }

        /// <summary>
        /// Posts the content.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Content>> PostContent(Content content)
        {
            Content entity = await service.Create(content);
            return CreatedAtAction("GetContent", new { id = entity.ID }, entity);
        }

        /// <summary>
        /// Deletes the content.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContent(int id)
        {
            return await service.Delete(id) ? Ok() : NotFound();
        }
    }
}
