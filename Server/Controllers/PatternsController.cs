﻿using Microsoft.AspNetCore.Mvc;
using RegularExpressionValidator.Shared.Models;
using RegularExpressionValidator.Server.Services;

namespace RegularExpressionValidator.Server.Controllers
{
    /// <summary>
    /// Patterns controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class PatternsController : ControllerBase
    {
        /// <summary>
        /// The pattern service
        /// </summary>
        private readonly PatternService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternsController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public PatternsController(PatternService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Gets the patterns.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Pattern>>> GetPatterns()
        {
            return await service.ListAll();
        }

        /// <summary>
        /// Gets the pattern.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Pattern>> GetPattern(int id)
        {
            Pattern pattern = await service.Read(id);
            return pattern == null ? NotFound() : Ok(pattern);
        }

        /// <summary>
        /// Puts the pattern.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> PutPattern(Pattern pattern)
        {
            return await service.Update(pattern) ? Ok(pattern) : NotFound();
        }

        /// <summary>
        /// Posts the pattern.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Pattern>> PostPattern(Pattern pattern)
        {
            Pattern entity = await service.Create(pattern);
            return CreatedAtAction("GetPattern", new { id = entity.ID }, entity);
        }

        /// <summary>
        /// Deletes the pattern.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePattern(int id)
        {
            return await service.Delete(id) ? Ok() : NotFound();
        }
    }
}
