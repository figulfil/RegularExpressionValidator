﻿using Microsoft.AspNetCore.Mvc;
using RegularExpressionValidator.Server.Services;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Controllers
{
    /// <summary>
    /// UserDetail controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class UserDetailsController : ControllerBase
    {
        /// <summary>
        /// The UserDetail service
        /// </summary>
        private readonly UserDetailService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDetailsController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public UserDetailsController(UserDetailService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Gets the user details.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDetail>>> GetUserDetails()
        {
            return await service.ListAll();
        }

        /// <summary>
        /// Gets the user detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDetail>> GetUserDetail(int id)
        {
            UserDetail userDetail = await service.Read(id);
            return userDetail == null ? NotFound() : Ok(userDetail);
        }

        /// <summary>
        /// Puts the user detail.
        /// </summary>
        /// <param name="userDetail">The user detail.</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> PutUserDetail(UserDetail userDetail)
        {
            return await service.Update(userDetail) ? Ok(userDetail) : NotFound();
        }

        /// <summary>
        /// Posts the user detail.
        /// </summary>
        /// <param name="userDetail">The user detail.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<UserDetail>> PostUserDetail(UserDetail userDetail)
        {
            UserDetail entity = await service.Create(userDetail);
            return CreatedAtAction("GetUserDetail", new { id = entity.ID }, entity);
        }

        /// <summary>
        /// Deletes the user detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserDetail(int id)
        {
            return await service.Delete(id) ? Ok() : NotFound();
        }

        /// <summary>
        /// Gets the user detail by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [HttpGet("User/{userId}")]
        public async Task<ActionResult<UserDetail>> GetUserDetailByUserId(int userId)
        {
            UserDetail UserDetail = await service.ReadByUserId(userId);
            return UserDetail == null ? NotFound() : Ok(UserDetail);
        }
    }
}
