﻿using Microsoft.EntityFrameworkCore;
using RegularExpressionValidator.Server.Data;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Repositories
{
    public class PatternRepository : IRepository<Pattern>
    {
        /// <summary>
        /// Gets the context of the database.
        /// </summary>
        public ApplicationDbContext Context { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public PatternRepository(ApplicationDbContext context)
        {
            this.Context = context;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<Pattern> Create(Pattern entity)
        {
            this.Context.Patterns.Add(entity);
            await this.Context.SaveChangesAsync();

            return entity;
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Pattern> Read(int id)
        {
            return await Context.Patterns.FindAsync(id);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public async Task Update(Pattern entity)
        {
            this.Context.Patterns.Update(entity);
            await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public async Task Delete(Pattern entity)
        {
            this.Context.Patterns.Remove(entity);
            await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Pattern>> ListAll()
        {
            return await Context.Patterns.ToListAsync();
        }
    }
}
