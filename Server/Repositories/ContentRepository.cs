﻿using Microsoft.EntityFrameworkCore;
using RegularExpressionValidator.Server.Data;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Repositories
{
    public class ContentRepository : IRepository<Content>
    {
        /// <summary>
        /// Gets the context of the database.
        /// </summary>
        public ApplicationDbContext Context { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ContentRepository(ApplicationDbContext context)
        {
            this.Context = context;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<Content> Create(Content entity)
        {
            this.Context.Contents.Add(entity);
            await this.Context.SaveChangesAsync();

            return entity;
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<Content> Read(int id)
        {
            return await Context.Contents.FindAsync(id);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public async Task Update(Content entity)
        {
            this.Context.Contents.Update(entity);
            await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public async Task Delete(Content entity)
        {
            this.Context.Contents.Remove(entity);
            await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Content>> ListAll()
        {
            return await Context.Contents.ToListAsync();
        }
    }
}
