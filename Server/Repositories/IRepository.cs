﻿using RegularExpressionValidator.Server.Data;

namespace RegularExpressionValidator.Server.Repositories
{
    /// <summary>
    /// Interface constraining basic CRUD operation used by repositories
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Gets the context of the database.
        /// </summary>
        public ApplicationDbContext Context { get; }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public Task<TEntity> Create(TEntity entity);

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Task<TEntity> Read(int id);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public Task Update(TEntity entity);

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public Task Delete(TEntity entity);

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public Task<List<TEntity>> ListAll();
    }
}
