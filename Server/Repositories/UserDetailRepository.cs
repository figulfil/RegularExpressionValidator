﻿using Microsoft.EntityFrameworkCore;
using RegularExpressionValidator.Server.Data;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Repositories
{
    public class UserDetailRepository : IRepository<UserDetail>
    {
        /// <summary>
        /// Gets the context of the database.
        /// </summary>
        public ApplicationDbContext Context { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDetailRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UserDetailRepository(ApplicationDbContext context)
        {
            this.Context = context;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<UserDetail> Create(UserDetail entity)
        {
            this.Context.UserDetails.Add(entity);
            await this.Context.SaveChangesAsync();

            return entity;
        }

        /// <summary>
        /// Reads the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<UserDetail> Read(int id)
        {
            return await Context.UserDetails.FindAsync(id);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public async Task Update(UserDetail entity)
        {
            this.Context.UserDetails.Update(entity);
            await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public async Task Delete(UserDetail entity)
        {
            this.Context.UserDetails.Remove(entity);
            await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Lists all entities.
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserDetail>> ListAll()
        {
            return await Context.UserDetails.ToListAsync();
        }

        /// <summary>
        /// Reads the by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public async Task<UserDetail> ReadByUserId(int userId)
        {
            return await Context.UserDetails.SingleOrDefaultAsync(detail => detail.UserID == userId);
        }
    }
}