﻿using Duende.IdentityServer.EntityFramework.Entities;
using Duende.IdentityServer.EntityFramework.Extensions;
using Duende.IdentityServer.EntityFramework.Interfaces;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RegularExpressionValidator.Server.Models;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Data
{
    /// <summary>
    /// Abstract DbContext implementing user with integer key
    /// </summary>
    /// <typeparam name="TUser">The type of the user.</typeparam>
    /// <typeparam name="TRole">The type of the role.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <seealso cref="Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityDbContext&lt;TUser, TRole, TKey&gt;" />
    /// <seealso cref="Duende.IdentityServer.EntityFramework.Interfaces.IPersistedGrantDbContext" />
    public abstract class DbContext<TUser, TRole, TKey> : IdentityDbContext<TUser, TRole, TKey>, IPersistedGrantDbContext
        where TUser : IdentityUser<TKey>
        where TRole : IdentityRole<TKey>
        where TKey : IEquatable<TKey>
    {
        private readonly IOptions<OperationalStoreOptions> operationalStoreOptions;
        Task<int> IPersistedGrantDbContext.SaveChangesAsync() => base.SaveChangesAsync();

        public DbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions)
            : base(options)
        {
            this.operationalStoreOptions = operationalStoreOptions;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ConfigurePersistedGrantContext(operationalStoreOptions.Value);
        }

        public DbSet<PersistedGrant> PersistedGrants { get; set; }
        public DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; }
        public DbSet<Key> Keys { get; set; }
    }

    /// <summary>
    /// Our custom DBContext used in our appliaction
    /// </summary>
    /// <seealso cref="RegularExpressionValidator.Server.Data.DbContext&lt;RegularExpressionValidator.Server.Models.User, RegularExpressionValidator.Server.Models.Role, System.Int32&gt;" />
    public class ApplicationDbContext : DbContext<User, Role, int>
    {
        public ApplicationDbContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions)
            : base(options, operationalStoreOptions) { }

        public DbSet<Content> Contents { get; set; }
        public DbSet<Pattern> Patterns { get; set; }
        public DbSet<UserDetail> UserDetails { get; set; }
    }
}