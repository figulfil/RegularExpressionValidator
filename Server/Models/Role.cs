﻿using Microsoft.AspNetCore.Identity;

namespace RegularExpressionValidator.Server.Models
{
    /// <summary>
    /// Entity representing user role.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.IdentityRole&lt;System.Int32&gt;" />
    public class Role : IdentityRole<int>
    {
        public string Description { get; set; }
    }
}
