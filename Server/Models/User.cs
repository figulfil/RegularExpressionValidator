﻿using Microsoft.AspNetCore.Identity;
using RegularExpressionValidator.Shared.Models;

namespace RegularExpressionValidator.Server.Models
{
    /// <summary>
    /// Entity representing user.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.IdentityUser&lt;System.Int32&gt;" />
    public class User : IdentityUser<int>
    {
        public UserDetail UserDetail { get; set; }
        public ICollection<Pattern> Patterns { get; set; }
        public ICollection<Content> Contents { get; set; }
    }
}